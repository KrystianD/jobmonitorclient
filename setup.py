from setuptools import setup

setup(
        name='jobmonitorclient',
        version='0.0.5',
        description='JobMonitor project client',
        author='Krystian Dużyński',
        author_email='krystian.duzynski@gmail.com',
        license='MIT',
        packages=["jobmonitorclient"],
        install_requires=[
            'requests',
        ],
        entry_points={
            'console_scripts': [
                'jobmonitorclient = jobmonitorclient.entry:entry',
            ],
        },
)
