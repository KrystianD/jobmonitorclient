import datetime


class ObjectNotExistsException(Exception):
    pass


class Application:
    def __init__(self):
        self.app_id = None  # type: str
        self.name = None  # type: str
        self.creation_date = None  # type: datetime.datetime
        self.last_modification_date = None  # type: datetime.datetime
        self.name = None  # type: str
        self.target_email = None  # type: str
        self.mailgun_apikey = None  # type: str
        self.mailgun_domain = None  # type: str
        self.pushover_apikey = None  # type: str
        # self.notification_interval_enabled = None  # type: int
        self.notification_interval = None  # type: int
        self.last_notification_time = None  # type: int

    def print(self):
        print("app_id", self.app_id)
        print("name", self.name)
        print("creation_date", self.creation_date)
        print("last_modification_date", self.last_modification_date)
        print("target_email", self.target_email)
        print("mailgun_apikey", self.mailgun_apikey)
        print("mailgun_domain", self.mailgun_domain)
        print("pushover_apikey", self.pushover_apikey)
        # print("notification_interval_enabled", self.notification_interval_enabled)
        print("notification_interval", self.notification_interval, )
        print("last_notification_time", self.last_notification_time)


class Job:
    def __init__(self):
        self.app_id = None  # type: str
        self.name = None  # type: str
        self.creation_date = None  # type: datetime.datetime
        self.last_modification_date = None  # type: datetime.datetime
        self.interval = None  # type: int
        self.grace_time = None  # type: int
        self.last_run_date = None  # type: datetime.datetime
        # self.notification_interval_enabled = None  # type: int
        self.notification_interval = None  # type: int
        self.last_notification_time = None  # type: int

    def print(self):
        print("app_id", self.app_id)
        print("name", self.name)
        print("creation_date", self.creation_date)
        print("last_modification_date", self.last_modification_date)
        print("interval", self.interval)
        print("grace_time", self.grace_time)
        print("last_run_date", self.last_run_date)
        # print("notification_interval_enabled", self.notification_interval_enabled)
        print("notification_interval", self.notification_interval, )
        print("last_notification_time", self.last_notification_time)
