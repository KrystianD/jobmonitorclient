import datetime
import logging
import requests
from requests import Response

from .models import Application, ObjectNotExistsException, Job

logger = logging.getLogger("jobmonitorclient")


class JobMonitorClient:
    def __init__(self, url="https://jobmonitor.kdapps.net"):
        self.url = url

    def create_app(self,
                   name: str,
                   create_only: bool = False,
                   target_email: str = None,
                   mailgun_apikey: str = None,
                   mailgun_domain: str = None,
                   pushover_apikey: str = None,
                   notification_interval: int = None):
        resp = self._do_post("api/apps".format(), {
            "name": name,
            "create_only": create_only,
            "target_email": target_email,
            "mailgun_apikey": mailgun_apikey,
            "mailgun_domain": mailgun_domain,
            "pushover_apikey": pushover_apikey,
            "notification_interval": notification_interval
        })
        assert resp.status_code == 200

        data = resp.json()
        return data

    def update_app(self,
                   app_id: str,
                   name: str,
                   create_only: bool = False,
                   target_email: str = None,
                   mailgun_apikey: str = None,
                   mailgun_domain: str = None,
                   pushover_apikey: str = None,
                   notification_interval: int = None):
        resp = self._do_put("api/apps/{0}".format(app_id), {
            "name": name,
            "create_only": create_only,
            "target_email": target_email,
            "mailgun_apikey": mailgun_apikey,
            "mailgun_domain": mailgun_domain,
            "pushover_apikey": pushover_apikey,
            "notification_interval": notification_interval
        })
        assert resp.status_code == 200

    def get_app(self, app_id: str) -> Application:
        resp = self._do_get("api/apps/{0}".format(app_id))

        if resp.status_code == 404:
            raise ObjectNotExistsException()

        assert resp.status_code == 200
        data = resp.json()

        app = Application()
        app.app_id = data["app_id"]
        app.name = data["name"]
        app.creation_date = JobMonitorClient._parse_date(data["creation_date"])
        app.last_modification_date = JobMonitorClient._parse_date(data["last_modification_date"])
        app.target_email = data["target_email"]
        app.mailgun_apikey = data["mailgun_apikey"]
        app.mailgun_domain = data["mailgun_domain"]
        app.pushover_apikey = data["pushover_apikey"]
        app.notification_interval = data["notification_interval"]
        # app.notification_interval_enabled = data["notification_interval_enabled"]
        app.last_notification_time = data["last_notification_time"]
        return app

    def create_job(self, app_id: str, job_name: str, interval: int, grace_time: int, notification_interval: int = None):
        resp = self._do_post("api/apps/{0}/jobs/{1}".format(app_id, job_name), {
            "interval": interval,
            "grace_time": grace_time,
            "notification_interval": notification_interval
        })
        logger.debug("creating job response code: {}".format(resp.status_code))
        logger.info("response text: {}".format(resp.text))

        if resp.status_code != 200:
            raise Exception(resp.text)

    def get_job(self, app_id: str, job_name: str) -> Job:
        resp = self._do_get("api/apps/{0}/jobs/{1}".format(app_id, job_name))

        if resp.status_code == 404:
            raise ObjectNotExistsException()

        assert resp.status_code == 200
        data = resp.json()

        job = Job()
        job.app_id = data["app_id"]
        job.name = data["name"]
        job.creation_date = JobMonitorClient._parse_date(data["creation_date"])
        job.last_modification_date = JobMonitorClient._parse_date(data["last_modification_date"])
        job.interval = data["interval"]
        job.grace_time = data["grace_time"]
        job.last_run_date = JobMonitorClient._parse_date(data["last_run_date"])
        job.notification_interval = data["notification_interval"]
        job.last_notification_time = JobMonitorClient._parse_date(data["last_notification_time"])
        job.notification_interval_enabled = data["notification_interval_enabled"]
        return job

    def notify_job(self, app_id: str, job_name: str):
        resp = self._do_put("api/apps/{0}/jobs/{1}".format(app_id, job_name))

        if resp.status_code == 404:
            raise ObjectNotExistsException()

        assert resp.status_code == 200
        data = resp.json()

    def _do_post(self, ep, data) -> Response:
        return requests.post(self._get_ep(ep), json=data, timeout=5)

    def _do_get(self, ep) -> Response:
        return requests.get(self._get_ep(ep), timeout=5)

    def _do_put(self, ep, data={}) -> Response:
        return requests.put(self._get_ep(ep), json=data, timeout=5)

    def _get_ep(self, name):
        return "{0}/{1}".format(self.url, name)

    @staticmethod
    def _parse_date(s: str) -> datetime.datetime:
        return datetime.datetime.strptime(s, "%Y-%m-%dT%H:%M:%S")
