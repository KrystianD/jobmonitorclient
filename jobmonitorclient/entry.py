import argparse
import sys

from jobmonitorclient import JobMonitorClient


def printinfo(txt, *args):
    print(txt, *args, file=sys.stderr)


def entry():
    parser = argparse.ArgumentParser()

    parser.add_argument('--url', type=str, default="https://jobmonitor.kdapps.net")

    subparsers = parser.add_subparsers(dest="cmd")
    subparsers.required = True

    # app parser
    parser_app = subparsers.add_parser('application', help='application')
    app_subparsers = parser_app.add_subparsers(dest="subcmd")

    parser_add = app_subparsers.add_parser('create', help='add application')
    parser_add.add_argument('--name', type=str, default="")
    parser_add.add_argument('--target-email', type=str, default=None)
    parser_add.add_argument('--mailgun-apikey', type=str, default=None)
    parser_add.add_argument('--mailgun-domain', type=str, default=None)
    parser_add.add_argument('--pushover-apikey', type=str, default=None)
    parser_add.add_argument('--notification-interval', type=int, default=None)

    parser_update = app_subparsers.add_parser('update', help='add application')
    parser_update.add_argument('app_id', type=str)
    parser_update.add_argument('--name', type=str, default="")
    parser_update.add_argument('--target-email', type=str, default=None)
    parser_update.add_argument('--mailgun-apikey', type=str, default=None)
    parser_update.add_argument('--mailgun-domain', type=str, default=None)
    parser_update.add_argument('--pushover-apikey', type=str, default=None)
    parser_update.add_argument('--notification-interval', type=int, default=None)

    parser_get = app_subparsers.add_parser('get', help='get application')
    parser_get.add_argument('app_id', type=str)

    # job parser
    parser_job = subparsers.add_parser('job', help='job')
    job_subparsers = parser_job.add_subparsers(dest="subcmd")
    parser_add = job_subparsers.add_parser('create', help='add job')
    parser_add.add_argument('app_id', type=str)
    parser_add.add_argument('name', type=str)
    parser_add.add_argument('--interval', type=int, required=True)
    parser_add.add_argument('--grace-time', type=int, required=True)
    parser_add.add_argument('--notification-interval', type=int, default=None)
    parser_get = job_subparsers.add_parser('get', help='get job')
    parser_get.add_argument('app_id', type=str)
    parser_get.add_argument('name', type=str)

    # notify parser
    parser_job = subparsers.add_parser('notify', help='notify')
    parser_job.add_argument('app_id', type=str)
    parser_job.add_argument('name', type=str)
    parser_job.add_argument('--notification-interval', type=int, default=None)

    args = parser.parse_args()

    client = JobMonitorClient(args.url)

    if args.cmd == "application":
        if args.subcmd == "create":
            app_id = client.create_app(
                    args.name,
                    target_email=args.target_email,
                    mailgun_apikey=args.mailgun_apikey,
                    mailgun_domain=args.mailgun_domain,
                    pushover_apikey=args.pushover_apikey,
                    notification_interval=args.notification_interval
            )
            printinfo("Application created successfully")
            print(app_id)

        if args.subcmd == "update":
            client.update_app(
                    args.app_id,
                    args.name,
                    target_email=args.target_email,
                    mailgun_apikey=args.mailgun_apikey,
                    mailgun_domain=args.mailgun_domain,
                    pushover_apikey=args.pushover_apikey,
                    notification_interval=args.notification_interval
            )
            printinfo("Application update successfully")

        if args.subcmd == "get":
            app = client.get_app(args.app_id)
            app.print()

    if args.cmd == "job":
        if args.subcmd == "create":
            client.create_job(args.app_id, args.name, args.interval, args.grace_time, args.notification_interval)
            printinfo("Job created successfully")

        if args.subcmd == "get":
            job = client.get_job(args.app_id, args.name)
            job.print()

    if args.cmd == "notify":
        client.notify_job(args.app_id, args.name)


if __name__ == "__main__":
    entry()
